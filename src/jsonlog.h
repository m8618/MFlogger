// Copyright 2024 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mflogger
//
// jsonlog.h: interface for the jsonlog class
//

#include <cassert>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <mflog.h>

#ifndef __MF_JSONLOG_H__
#define __MF_JSONLOG_H__

namespace mflogger {

class jsonlog: public log {
 public:
  // default time prefix ISO8601: YYYY-MM-DDTHH:MM:SS
  //                          e.g 1983-04-17T14:23:05
  // see std::put_time for format string
  jsonlog(const std::string& logfilename, const bool& append = true, timestring::format prefix = timestring::format::iso8601);
  ~jsonlog();

  // log message with timestamp/prefix. return the whole log line
  // e.g.  general       : TRACE, DEBUG, INFO, WARN, ERROR, FATAL.
  //       RSYSLOG style : emerg alert crit error warn notice info debug
  // don't add Level if no level is appended
  std::string logjson(const std::string& logitem, const std::string& value, const std::string& level = "");

  // Level can obviously be anything that parses to valid json name
  // e.g.  general       : TRACE, DEBUG, INFO, WARN, ERROR, FATAL.
  //       RSYSLOG style : emerg alert crit error warn notice info debug
  // don't add Level if no level is appended
  std::string logjson(const std::vector<std::pair<std::string, std::string> >& logdata, const std::string& level = "");
 
 private:
  // disallow copying
  jsonlog(const jsonlog&);
  jsonlog& operator= (const jsonlog& log);
};

} // end namespace mflogger

#endif // !ifndef __MF_JSONLOG_H__

