// Copyright 2024 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mflogger
//
// jsonlog.cpp: implementation for the json logging class
//

#include <jsonlog.h>

namespace mflogger {

jsonlog::jsonlog(const std::string& logfilename, const bool& append, timestring::format prefix):log(logfilename, append, prefix) { }
jsonlog::~jsonlog() {  }

// log message with timestamp/prefix. return the whole log line
// e.g.  general       : TRACE, DEBUG, INFO, WARN, ERROR, FATAL.
//       RSYSLOG style : emerg alert crit error warn notice info debug
std::string jsonlog::logjson(const std::string& logitem, const std::string& value, const std::string& level) {
  assert(logfile.is_open());

  std::ostringstream ret; // return empty string if file not open for writing
  if(logfile.is_open()) {
    if(!level.length()) {
      ret << "{\"timestamp\":\"" << timestring::time_asstring() << ",\"" << logitem << "\":\"" << value << "\"}";
    }
    else {
      ret << "{\"level\":\"" << level << "\",\"timestamp\":\"" << timestring::time_asstring() << ",\"" << logitem << "\":\"" << value << "\"}";
    }
    logfile << ret.str() << std::endl;
  }
  return ret.str();
}

// Level can obviously be anything that parses to valid json name
// e.g.  general       : TRACE, DEBUG, INFO, WARN, ERROR, FATAL.
//       RSYSLOG style : emerg alert crit error warn notice info debug
std::string jsonlog::logjson(const std::vector<std::pair<std::string, std::string> >& logdata, const std::string& level) {
  assert(logfile.is_open());

  std::ostringstream ret; // return empty string if file not open for writing
  if(logfile.is_open()) {
    if(!level.length()) {
      ret << "{\"timestamp\":\"" << timestring::time_asstring() << std::string((logdata.size())?"\",":"\"");
    }
    else {
      ret << "{\"level\":\"" << level << "\",\"timestamp\":\"" << timestring::time_asstring() << std::string((logdata.size())?"\",":"\"");
    }
    for(std::vector<std::pair<std::string, std::string> >::const_iterator i(logdata.begin()); i < logdata.end(); ++i) {
      ret << "\"" << std::get<0>(*i) << "\":\"" << std::get<1>(*i) << std::string((i + 1 != logdata.end())?"\",":"\"");
      //if(i + 1 != logdata.end())
      //  ret << "\"," << std::endl;
      //else
      //  ret << "\"" << std::endl;
    }
    ret << "}";
    logfile << ret.str() << std::endl;
  }
  return ret.str();  
}

} // end namespace mflogger
