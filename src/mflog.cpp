// Copyright 2024 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mflogger
//
// log.cpp: implementation for the log class
//

#include <mflog.h>

namespace mflogger {

log::log(const std::string& logfilename, const bool& append, timestring::format prefix): logfilename(logfilename), delimiter(" ") {
  if(logfilename.length())
    open(logfilename, append);

  timeformat(prefix);
}

log::~log() {
   logfile.close(); // not necassary, just seems write to do this.
}

bool log::open(const std::string& logFilename, const bool& append) {
  logfile.close(); // be a good citizen
  logfile.clear(); // clean up flags
  logfilename = logFilename;

  if(!logFilename.length()) // oops
    return false;

  if(append)
    logfile.open(logfilename.c_str(), std::ios_base::binary | std::ios_base::out | std::ios_base::app);
  else
    logfile.open(logfilename.c_str(), std::ios_base::binary | std::ios_base::out);

  return logfile.is_open();
}

std::string log::logmessage(const std::string& message, const std::string& delimiter) {
  assert(logfile.is_open());
  assert(message.length());
  std::string ret; // return empty string if file not open for writing

  if(logfile.good()) {
    ret = timestring::time_asstring() + delimiter + message;
    logfile << ret << std::endl;
    // logfile.flush();
  }
  return ret;
}

std::string log::logmessage_epoch(const std::string& message, timestring::precision p, const std::string& epoch_delimiter) {
  assert(logfile.is_open());
  assert(message.length());
  std::string ret; // return empty string if file not open for writing

  ret = timestring::epoch_asstring(p, epoch_delimiter) + std::string(" ") + message;
  logfile << ret << std::endl;
  logfile.flush();
  
  return ret;
}

void log::setdelimiter(const std::string& d) {
  delimiter = d;
}

bool log::good() const {
  return logfile.good();
}
bool log::is_open() const {
  return logfile.is_open();
}

log& log::operator<<(const char* in) {   
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
    // logfile.flush();
  }
  return *this;
}
log& log::operator<<(const std::string& in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
    // logfile.flush();
  }
  return *this;
}
log& log::operator<<(bool in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << ((in)?"true":"false") << std::endl;
    // logfile.flush();
  }
  return *this;
}
log& log::operator<<(char in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
    // logfile.flush();
  }
  return *this;
}
log& log::operator<<(int in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
    // logfile.flush();
  }
  return *this;
}
log& log::operator<<(short in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
    // logfile.flush();
  }
  return *this;
}
log& log::operator<<(long in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
    // logfile.flush();
  }
  return *this;
}
log& log::operator<<(float in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(double in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(long double in) {
  if(logfile.good()) {
   logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(long long in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(unsigned char in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(unsigned int in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(unsigned short in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(unsigned long in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}
log& log::operator<<(unsigned long long in) {
  if(logfile.good()) {
    logfile << time_asstring() << delimiter << in << std::endl;
  }
  return *this;
}

} // end namespace mflogger
