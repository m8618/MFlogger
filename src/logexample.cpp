// Copyright 2024 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mflogger
//
// logexample.cpp: logging demo
//

#include <iostream>
#include <iomanip>
#include <mflogger.h>


//#include "src/mflog.h"
//#include "src/jsonlog.h"
//#include "src/timestring.h"

int main(int argc, char* argv[], char *envp[]) {
  const std::string logfilename("logexample.log");
  mflogger::log logger(logfilename, false); // create/overwrite file

  if(logger.good()) { // log file created/opened ok
    logger.logmessage("line one logging with time as an ISO8601 string");
	   
    // set a new prefix string
    logger.timeformat("%b %d %T mflogger:");  // e.g. Feb 21 18:32:05 mflogger:
    logger.logmessage("changed the time format");
    
    logger.logmessage_epoch("line three logging seconds since epoch", mflogger::timestring::precision::sec);

    logger.timeformat("%T - bool test]"); // test bool
    logger << "positive bool test on next line" << true << "negative bool test on next line" << false;
    
    logger << "Change the delimiter using setdelimiter(\" ]_[ \")\n";
    logger.setdelimiter(" ]_[ ");

    // set a new prefix string again
    logger.timeformat(mflogger::timestring::format::iso8601);
    logger << "Streaming values with c++ insertion operator (<<):-\n  i.e.\n   logger << std::string(\"hello log\") << 8 << 34.6f << 'R' << 1200000000340000.45"
        << std::string("hello log") << 8 << 34.6f << 'R' << 1200000000340000.45L;
  }
  else {
    std::cout << "Oops, could not create/open log file - '" << logfilename << "'\n";
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // test timestring class
  mflogger::timestring st;
  std::cout << "timestring class examples\n  time_asstring() " << std::string(st.time_asstring()) << std::endl;
  std::cout << "  ISO8601TimeUTC() " << std::string(st.ISO8601TimeUTC()) << std::endl;
  st.timeformat(mflogger::timestring::format::HMS12);
  std::cout << "  12 hour " << st.time_asstring() << std::endl;
  st.timeformat(mflogger::timestring::format::HMS24);
  std::cout << "  24 hour " << st.time_asstring() << std::endl;

  std::cout << "epoch_asstring() examples" << std::endl;
  // example using epoch_asstring
  std::string sec(st.epoch_asstring(mflogger::timestring::precision::sec));
  std::string milli(st.epoch_asstring(mflogger::timestring::precision::milli));
  std::string micro(st.epoch_asstring(mflogger::timestring::precision::micro));
  std::string nano(st.epoch_asstring(mflogger::timestring::precision::nano));
  std::string sec_milli(st.epoch_asstring(mflogger::timestring::precision::sec_milli));
  std::string sec_micro(st.epoch_asstring(mflogger::timestring::precision::sec_micro));
  std::string sec_nano(st.epoch_asstring(mflogger::timestring::precision::sec_nano));
 
  std::cout << std::setw(22) << std::right << "seconds: " << std::left << sec << std::endl;
  std::cout << std::setw(22) << std::right << "milliseconds: " << std::left << milli << std::endl;
  std::cout << std::setw(22) << std::right << "microseconds: " << std::left << micro << std::endl;
  std::cout << std::setw(22) << std::right << "nanoseconds: " << std::left << nano << std::endl;
  std::cout << std::setw(22) << std::right << "seconds.milliseconds: " << std::left << sec_milli << std::endl;
  std::cout << std::setw(22) << std::right << "seconds.microseconds: " << std::left << sec_micro << std::endl;
  std::cout << std::setw(22) << std::right << "seconds.nanoseconds: " << std::left << sec_nano << std::endl << std::endl;

  // json logging examples
  mflogger::jsonlog jlog("jsonlog.log", true);
  if(!jlog.is_open()) {
    std::cerr << "ERROR opening: " << "jsonlog.txt" << std::endl;
  }
  else {
    std::string lj1(jlog.logjson("name", "harold", "debug"));

    std::vector<std::pair<std::string, std::string> > values;
    values.push_back(std::make_pair("hello", "world"));
    values.push_back(std::make_pair("pointless", "query"));
    values.push_back(std::make_pair("pi", "3.14159265358979323846"));

    jlog.timeformat(mflogger::timestring::format::iso8601Z); // add timezone to the log line
    std::string lj2(jlog.logjson(values, "trace"));
    
    std::cout << lj1 << "\n" << lj2 << "\n";

    // without level
    std::string lj3(jlog.logjson(values));
    std::cout << "without LEVEL: " << lj3 << "\n\n" ;
    
    std::cout << "LOGGED to jsonlog.log" << std::endl;
  }

  return 0;
}
