// Copyright 2024 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mflogger
//
// log.h: interface for the log class
//

#include <cassert>
#include <fstream>
#include <sstream>
#include <string>

#include <timestring.h>

#ifndef __MF_LOG_H__
#define __MF_LOG_H__

namespace mflogger {

class log: public timestring {
 public:
   // default time prefix ISO8601: YYYY-MM-DDTHH:MM:SS
   //                          e.g 1983-04-17T14:23:05
   // see std::put_time for format string
   log(const std::string& logfilename, const bool& append = true, timestring::format prefix = timestring::format::iso8601);
   ~log();

   // open/create log file for logging
   bool open(const std::string& logfilename, const bool& append = true);

   // log message with timestamp/prefix. return the whole log line
   std::string logmessage(const std::string& message, const std::string& delimeter = " ");
   std::string logmessage_epoch(const std::string& message, timestring::precision p = timestring::precision::sec, const std::string& epoch_delimeter = ".");
   
   void setdelimiter(const std::string& d);

   log& operator<<(const char* in);
   log& operator<<(const std::string& in);
   log& operator<<(bool in);
   log& operator<<(char in);
   log& operator<<(int in);
   log& operator<<(short in);
   log& operator<<(long in);
   log& operator<<(float in);
   log& operator<<(double in);
   log& operator<<(long double in);
   log& operator<<(long long in);
   log& operator<<(unsigned char in);
   log& operator<<(unsigned int in);
   log& operator<<(unsigned short in);
   log& operator<<(unsigned long in);
   log& operator<<(unsigned long long in);

  // in case the underlying file object is needed
  //std::ofstream& getfile() { return logfile; }
  
  // is open ready for log input [encapsulates std:: library routines]
  bool good() const;
  bool is_open() const;

 protected:
   std::string logfilename;
   std::string delimiter;
   std::ofstream logfile;

   // disallow copying
   log(const log&);
   log& operator= (const log& log);
};

} // end namespace mflogger

// bring in the implementation
//#include "log.cpp"

#endif // !ifndef __MF_LOG_H__

