// Copyright 2024 www.mumbleface.net  -- john wiggins
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
//    and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
// USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   https://www.mumbleface.net: mflogger
//
// timestring.h: interface/implementation for the class timestring
//

#include <cassert>
#include <iomanip>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>

#ifndef __MF_STRINGTIME_H__
#define __MF_STRINGTIME_H__

namespace mflogger {

class timestring {
 public:
 
  // local time | iso 8601 with timezone name | timezone offset from UTC | 24 hour clock | 12 hour clock
  enum format {iso8601, iso8601Z, iso8601z, HMS24, HMS12};

  // precision used to format string in epoch_asstring function
  enum precision {sec, milli, micro, nano, sec_milli, sec_micro, sec_nano};
  
  // ISO 8601 local as default string returned
  timestring(timestring::format f = timestring::format::iso8601) { 
    timeformat(f);
  }
  
  std::string time_asstring() { return gettime(); }

  // time since epoch in various formats
  // enum precision shows formatting possible
  //   e.g. sec_nano =  seconds<delimeter>nanoseconds
  //        timestring st;
  //        st.epoch_asstring(mflogger::timestring::precision::sec_nano);
  //
  // delimeter can any valid std::string value
  std::string epoch_asstring(precision Precision = sec, std::string delimeter = ".") {
    const auto tp = std::chrono::system_clock::now();
    const auto e = tp.time_since_epoch();
    std::ostringstream ret;
    // auto Milli = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    // auto Micro = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    // auto Nano = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    // auto Seconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    switch(Precision)  {
      case milli: {
        auto Milli = std::chrono::duration_cast<std::chrono::milliseconds>(e).count();
        ret << Milli;
        break;
      }
      case micro: {
        auto Micro = std::chrono::duration_cast<std::chrono::microseconds>(e).count();
        ret << Micro;
        break;
      }
      case nano: {
        auto Nano = std::chrono::duration_cast<std::chrono::nanoseconds>(e).count();
        ret << Nano;
        break;
      }
      case sec_milli: {
        auto Seconds = std::chrono::duration_cast<std::chrono::seconds>(e).count();
        auto Milli = std::chrono::duration_cast<std::chrono::milliseconds>(e).count();
        ret << Seconds << delimeter << (Milli % Seconds);
        break;
      }
      case sec_micro: {
        auto Seconds = std::chrono::duration_cast<std::chrono::seconds>(e).count();
        auto Micro = std::chrono::duration_cast<std::chrono::microseconds>(e).count();
        ret << Seconds << delimeter << (Micro % Seconds);
        break;
      }
      case sec_nano: {
        auto Seconds = std::chrono::duration_cast<std::chrono::seconds>(e).count();
        auto Nano = std::chrono::duration_cast<std::chrono::nanoseconds>(e).count();
        ret << Seconds << delimeter << (Nano % Seconds);
        break;
      }
      default: { // make the default seconds only
        auto Seconds = std::chrono::duration_cast<std::chrono::seconds>(e).count();
        ret << Seconds;
      }
    }
    return ret.str();
  }
  
  std::string ISO8601TimeUTC() {
    const auto tp = std::chrono::system_clock::now();
    const auto tt = std::chrono::system_clock::to_time_t(tp);

    std::ostringstream s;
    s << std::put_time(gmtime(&tt), "%FT%TZ");
    return s.str();
  }
  
  // Change the time/date formatting
  void timeformat(const std::string& FORMAT) { // see std::put_time to reference substitutions possible
    put_time_Format = FORMAT;
  }
  void timeformat(format FORMAT) {
    switch(FORMAT)  {
      case iso8601z: {  // if timezone is not available will be local time anyway
        put_time_Format = "%FT%T%z";
        break;
      }
      case iso8601Z: {
        put_time_Format = "%FT%T%Z";
        break;
      }
      case HMS24: {
        put_time_Format = "%T";
        break;
      }
      case HMS12: {
        put_time_Format = "%r";
        break;
      }
      default: { // make the default iso 8601 - local time
        put_time_Format = "%FT%T";
      }
    }
  }

 private:
  std::string gettime() { // convert to use std::chrono functions/declarations
    const auto tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

    std::ostringstream s;
    s << std::put_time(std::localtime(&tt), put_time_Format.c_str());//
    
    // C++ 20 and above
    //   s << std::chrono::zoned_time lt{ std::chrono::current_zone(), std::chrono::system_clock::now() };
    //   s << lt;
    return s.str();
  }

  // std::chrono::time_point<std::chrono::system_clock> tp;  // referencing what a time point variable looks like
  std::string put_time_Format;
};

} // end namespace mflogger

#endif // !ifndef __MF_STRINGTIME_H__